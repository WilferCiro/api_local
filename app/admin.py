from django.contrib import admin
from app.models import *

# Register your models here.
admin.site.register(Banner)
admin.site.register(TipoEmpresa)
admin.site.register(Pais)
admin.site.register(Departamento)
admin.site.register(Ciudad)
admin.site.register(Empresa)
admin.site.register(Producto)
admin.site.register(ProductoFoto)
admin.site.register(ProductoVideo)
