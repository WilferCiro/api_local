from rest_framework import status
from rest_framework.response import Response

noModelo = {"modelo" : ['No se ha indicado el modelo']}
noTipoServicio = {"tipo_servicio" : ['No se ha indicado el tipo_servicio']}
noBody = {"body" : ['No se ha enviado el cuerpo de la petición']}
errorBody = {"body" : ['El cuerpo enviado tiene errores.']}
errorTipoServicio = {"tipo_servicio" : ['No existe el servicio indicado.']}
errorOperacion = {"operacion" : ['No existe la operación a realizar.']}
errorModelo = {"operacion" : ['No existe el modelo a operar.']}
noOperacion = {"operacion" : ['No se ha indicado la operación a realizar.']}

noPOST = {"post" : ['El servicio no soporta el método POST.']}
noGET = {"get" : ['El servicio no soporta el método GET.']}
noPUT = {"put" : ['El servicio no soporta el método PUT.']}
noDELETE = {"delete" : ['El servicio no soporta el método DELETE.']}

creadoExito = {"ok"}


def sendResponse(response, myStatus=status.HTTP_200_OK, paginator = None):
	newResponse = {
		"estado_p" : myStatus,
		"data" : response
	}
	if paginator != None:
		newResponse["paginator"] = paginator
	return Response(newResponse, status=myStatus)
