from app.views.public.main import mainView
from rest_framework import serializers
from app.models import *
from datetime import date

# Custom serializers

# Custom View
class paisView(mainView):
	tabla = Pais
	defGet = {
		"todo" : {
			"campos" : ["pk", "nombre", "postal"],
			"external_conditions" : ["pk"]
		}
	}
