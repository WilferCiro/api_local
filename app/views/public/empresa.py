from app.views.public.main import mainView
from rest_framework import serializers
from app.models import *

# Custom serializers
class EmpresaTodoSerializer(serializers.ModelSerializer):
	ciudad__nombre = serializers.ReadOnlyField(source = 'ciudad.nombre_departamento')
	tipoempresa__nombre = serializers.ReadOnlyField(source = 'tipo_empresa.nombre')

	class Meta:
		model = Empresa
		fields = ["id", "ciudad__nombre", "tipoempresa__nombre", "nombre", "descripcion", "whatsapp", "celular", "email", "facebook", "twitter", "instagram", "fecha_union", "foto_perfil", "portada", "direccion"]

# Custom View
class empresaView(mainView):
	tabla = Empresa
	defGet = {
		"nombres" : {
			"campos" : ["pk", "nombre"],
			"external_conditions" : ["pk"]
		},
		"fotos" : {
			"campos" : ["pk", "nombre", "foto_perfil"],
			"external_conditions" : ["pk"]
		},
		"todo" : {
			"serializer" : EmpresaTodoSerializer,
			"external_conditions" : ["pk"]
		}
	}
