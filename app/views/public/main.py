from rest_framework import serializers
from app.models import *
from rest_framework.views import APIView
import json
from rest_framework import pagination

## Custom libraries
from app.views.utils.responses import *

class PostSerializer(serializers.ModelSerializer):
	class Meta:
		model = None

class GeneralSerializer(serializers.ModelSerializer):
	class Meta:
		model = None
		abstract = True

def getGeneralSerializer(model_arg):
	class GeneralSerializer(serializers.ModelSerializer):
		class Meta:
			model = model_arg
			abstract = True

	return GeneralSerializer

class StandardResultsSetPagination(pagination.PageNumberPagination):
	page_size = 2
	max_page_size = 1000

	def get_paginated_response(self, data):
		paginator = {
			'num_pages': self.page.paginator.num_pages,
			'count': self.page.paginator.count
		}
		return Response(data), paginator


class mainView(APIView):
	pagination_class = StandardResultsSetPagination

	def getFirst(self):
		return [self.tabla.objects.filter(**self.fields).order_by(self.order_by)[0]]

	def getLast(self):
		return [self.tabla.objects.filter(**self.fields).order_by(self.order_by)[self.tabla.objects.filter(**self.fields).count()-1]]

	def getTodos(self):
		return self.tabla.objects.filter(**self.fields).order_by(self.order_by)

	def getNumber(self):
		return self.tabla.objects.filter(**self.fields).order_by(self.order_by)[0:self.cantidad]

	def get(self, request):
		#### Check method
		if self.defGet == None:
			return sendResponse(noGET, status.HTTP_405_METHOD_NOT_ALLOWED)

		#### Get body
		self.body = request.GET.get("body", None)
		if self.body == None:
			return sendResponse(noBody, status.HTTP_400_BAD_REQUEST)
		try:
			self.body = json.loads(self.body)
		except Exception as e:
			return sendResponse(errorBody, status.HTTP_400_BAD_REQUEST)

		#### Check modelo
		if not ("modelo" in self.body and self.body["modelo"] not in ["", None]):
			return sendResponse(noModelo, status.HTTP_400_BAD_REQUEST)
		modeloStr = self.body["modelo"]

		if modeloStr not in self.defGet:
			return sendResponse(errorModelo, status.HTTP_400_BAD_REQUEST)
		fieldsGET = self.defGet[modeloStr]

		if "serializer" in fieldsGET:
			serializerCustom = fieldsGET["serializer"]
		else:
			serializerCustom = getGeneralSerializer(self.tabla)
			#serializerCustom.Meta.model = self.tabla
			serializerCustom.Meta.fields = fieldsGET["campos"]

		#### Generic variables
		self.filters = None
		self.fields = {}
		self.order_by = "pk"

		#### Conditions
		acceptConditions = fieldsGET["external_conditions"] if "external_conditions" in fieldsGET else False
		initialFields = fieldsGET["conditions"] if "conditions" in fieldsGET else {}
		self.order_by = self.body["ordenar_por"] if "ordenar_por" in self.body else "pk"

		if type(acceptConditions) == list and len(acceptConditions) > 0:
			bodyFields = self.body["campos"] if "campos" in self.body else {}
			for key in bodyFields:
				key_new = key.replace("-contiene", "").replace("-mayor_igual_que", "").replace("-mayor_que", "").replace("-menor_igual_que", "").replace("-menor_que", "").replace("-en", "")
				if key_new in acceptConditions:
					initialFields[key] = bodyFields[key]

		for key in initialFields:
			key_new = key.replace("-contiene", "__icontains").replace("-mayor_igual_que", "__gte").replace("-mayor_que", "__gt").replace("-menor_igual_que", "__lte").replace("-menor_que", "__lt").replace("-en", "__in")
			self.fields[key_new] = initialFields[key]

		queryset = None
		self.cantidad = None
		if "cantidad" in self.body:
			self.cantidad = self.body["cantidad"]

		queryset = self.getLogic()
		paginatorData = None

		estado = status.HTTP_200_OK
		if queryset.count() == 0:
			estado = status.HTTP_404_NOT_FOUND
		if "paginate" in self.body and self.body["paginate"] == True:
			page = self.paginate_queryset(queryset)
			if page is not None:
				serializer, paginatorData = self.get_paginated_response(serializerCustom(page, many=True).data)
				paginatorData["current"] = 1
				currentPage = request.GET.get("page", None)
				if currentPage != None:
					paginatorData["current"] = int(currentPage)
			else:
				serializer = serializerCustom(queryset, many=True, context={"request": request})
		else:
			serializer = serializerCustom(queryset, many=True, context={"request": request})
		return sendResponse(serializer.data, estado, paginatorData)

	@property
	def paginator(self):
		if not hasattr(self, '_paginator'):
			if self.pagination_class is None:
				self._paginator = None
			else:
				self._paginator = self.pagination_class()
		else:
			pass
		return self._paginator

	def paginate_queryset(self, queryset):
		if self.paginator is None:
			return None
		return self.paginator.paginate_queryset(queryset, self.request, view=self)

	def get_paginated_response(self, data):
		assert self.paginator is not None
		return self.paginator.get_paginated_response(data)

	def getLogic(self):
		queryset = None
		if self.cantidad != None:
			if type(self.cantidad) == int or self.cantidad.isnumeric():
				self.cantidad = int(self.cantidad)
				queryset = self.getNumber()
			elif self.cantidad == "ultimo":
				queryset = self.getLast()
			elif self.cantidad == "primero":
				queryset = self.getFirst()
			elif self.cantidad == "todos":
				queryset = self.getTodos()

		if queryset == None:
			queryset = self.getTodos()
		return queryset
