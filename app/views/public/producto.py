from app.views.public.main import mainView
from rest_framework import serializers
from app.models import *

# Custom serializers
class ProductImageSerializer(serializers.ModelSerializer):
	class Meta:
		model = ProductoFoto
		fields = ["foto", "descripcion"]

class ProductVideoSerializer(serializers.ModelSerializer):
	class Meta:
		model = ProductoVideo
		fields = ["url"]

class DatosBasicosProductoEmpresaSerializer(serializers.ModelSerializer):
	empresa__nombre = serializers.ReadOnlyField(source = 'empresa.nombre')
	empresa__pk = serializers.ReadOnlyField(source = 'empresa.pk')
	#fotos = ProductImageSerializer(many=True)
	fotos = serializers.SerializerMethodField()
	class Meta:
		model = Producto
		fields = ("pk", "nombre", "precio_iva", "presentacion", "fotos", "marca", "empresa__nombre", "empresa__pk")

	def get_fotos(self, obj):
		print(obj)
		qs = ProductoFoto.objects.filter(producto=obj).order_by('-principal')[:2]
		return ProductImageSerializer(instance=qs, many=True).data


# Custom View
class productoView(mainView):
	tabla = Producto
	defGet = {
		"datos_basicos" : {
			"serializer" : DatosBasicosProductoEmpresaSerializer,
			"conditions" : {"activo" : True},
			"external_conditions" : ["empresa__pk", "fecha_creacion"]
		}
	}
