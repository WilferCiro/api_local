from app.views.public.main import mainView
from rest_framework import serializers
from app.models import *

# Custom serializers


# Custom View
class tipoempresaView(mainView):
	tabla = TipoEmpresa
	defGet = {
		"nombres" : {
			"campos" : ["pk", "nombre"],
			"external_conditions" : ["pk"]
		},
		"todo" : {
			"campos" : ["pk", "nombre", "descripcion", "icono", "color"],
			"external_conditions" : ["pk"]
		}
	}
