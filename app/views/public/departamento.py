from app.views.public.main import mainView
from rest_framework import serializers
from app.models import *
from datetime import date

# Custom serializers
class SerializerTodo(serializers.ModelSerializer):
	pais__nombre = serializers.ReadOnlyField(source = 'pais.nombre')

	class Meta:
		model = Departamento
		fields = ["pk", "nombre", "pais__nombre"]

# Custom View
class departamentoView(mainView):
	tabla = Departamento
	defGet = {
		"todo" : {
			"serializer" : SerializerTodo,
			"external_conditions" : ["pk", "pais__pk"]
		}
	}
