from app.views.public.main import mainView
from rest_framework import serializers
from app.models import *
from datetime import date

# Custom serializers
class SerializerTodo(serializers.ModelSerializer):
	departamento__nombre = serializers.ReadOnlyField(source = 'departamento.nombre')

	class Meta:
		model = Ciudad
		fields = ["pk", "nombre", "postal", "departamento__nombre"]

# Custom View
class ciudadView(mainView):
	tabla = Ciudad
	defGet = {
		"todo" : {
			"serializer" : SerializerTodo,
			"external_conditions" : ["pk", "departamento__pk"]
		}
	}
