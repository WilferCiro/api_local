from app.views.public.main import mainView
from rest_framework import serializers
from app.models import *
from datetime import date

# Custom serializers


# Custom View
class bannerView(mainView):
	tabla = Banner
	defGet = {
		"activos" : {
			"campos" : ["pk", "descripcion", "imagen", "url", "tipo_banner"],
			"conditions" : {"activo": True, "fecha_inicio-menor_igual_que": date.today().strftime("%Y-%m-%d"), "fecha_fin-mayor_igual_que" : date.today().strftime("%Y-%m-%d")},
			"external_conditions" : ["tipo_banner"]
		}
	}
