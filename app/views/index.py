from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class indexView(APIView):

	def sendResponse(self, response, myStatus=status.HTTP_200_OK):
		newResponse = {
			"estado_p" : myStatus,
			"data" : response
		}
		return Response(newResponse, status=myStatus)

	def get(self, request):
		return self.sendResponse({"running": "La API de Local está corriendo satisfactoriamente"}, status.HTTP_200_OK)
