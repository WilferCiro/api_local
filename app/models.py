from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import AbstractUser
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill

#### Ubicaciones
class Pais(models.Model):
	nombre = models.CharField(max_length = 50)
	postal = models.CharField(max_length = 10)

	def __str__(self):
		return self.nombre + "-" + self.postal

class Departamento(models.Model):
	nombre = models.CharField(max_length = 50)
	pais = models.ForeignKey(Pais, on_delete=models.CASCADE, null=True)

	def __str__(self):
		return self.nombre

class Ciudad(models.Model):
	nombre = models.CharField(max_length = 50)
	postal = models.CharField(max_length = 10)
	departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE, null=True)

	def __str__(self):
		return self.nombre + "-" + self.postal

	def nombre_departamento(self):
		return self.nombre + " - " + self.departamento.nombre


#### User
class User(AbstractUser):
	fecha_creacion = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	celular = models.CharField(max_length=12, null = True, blank = True)
	direccion = models.TextField(null=True, blank=True)
	bloqueado = models.BooleanField("¿Bloqueado?", default=False)
	#avatar = models.ImageField(upload_to='avat/%s', null=True, blank = True)
	avatar = ProcessedImageField(upload_to='av/%s',
		processors=[ResizeToFill(200, 200)],
		format='WEBP',
		options={'quality': 100},
		null = True,
		blank = True)

	def __str__(self):
		return self.get_full_name()


### Banner modelo
class Banner(models.Model):
	fecha_inicio = models.DateField(null=True)
	fecha_fin = models.DateField(null=True)
	descripcion = models.TextField(null=True, blank=True)
	imagen = ProcessedImageField(upload_to='banner/%s',
		format='WEBP',
		options={'quality': 90},
		blank = False, null = False)
	url = models.URLField(null=True, blank=True, max_length = 150)
	activo = models.BooleanField("¿Activo?", default=True)

	TIPO_CHOOSE = (
		("H-1", "Banner principal"),
		("H-2", "Banner secundario 1"),
		("H-3", "Banner secundario 2"),
		("H-P", "Promociones")
	)
	tipo_banner = models.CharField(
		max_length = 5,
		choices = TIPO_CHOOSE,
		default = "H-1",
	)

	def __str__(self):
		return self.descripcion

##### Tipos de empresas
class TipoEmpresa(models.Model):
	nombre = models.CharField(max_length = 30)
	descripcion = models.TextField(null=True, blank=True)
	icono = models.ImageField(upload_to='tl/%s', blank = True, null = True)
	color = models.CharField(max_length = 10)

	def __str__(self):
		return self.nombre

### Empresa
class Empresa(models.Model):
	nombre = models.CharField(max_length = 100)
	descripcion = models.TextField(null=True, blank=True)
	whatsapp = models.CharField(max_length = 12, null=True, blank=True)
	celular = models.CharField(max_length = 12, null=True, blank=True)
	email = models.EmailField(max_length = 254, null=False, blank=False)
	facebook = models.URLField(max_length = 50, null=True, blank=True)
	twitter = models.URLField(max_length = 50, null=True, blank=True)
	instagram = models.URLField(max_length = 50, null=True, blank=True)
	fecha_union = models.DateTimeField(auto_now_add = True)
	tipo_empresa = models.ForeignKey(TipoEmpresa, on_delete=models.SET_NULL, null=True, blank=True)

	foto_perfil = ProcessedImageField(upload_to='empresa/pp/%s',
		processors=[ResizeToFill(400, 400)],
		format='WEBP',
		options={'quality': 90},
		blank = True, null = True)
	portada = ProcessedImageField(upload_to='empresa/pb/%s',
		processors=[ResizeToFill(1600, 340)],
		format='WEBP',
		options={'quality': 90},
		blank = True, null = True)

	direccion = models.TextField(null=True, blank=True)
	ciudad = models.ForeignKey(Ciudad, on_delete=models.SET_NULL, null=True, blank=True)

	#ubicacion

	def __str__(self):
		return self.nombre

class UserEmpresa(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, null=True)

	def __str__(self):
		return str(self.user) + " - " + str(self.empresa)

###

class Producto(models.Model):
	nombre = models.CharField(max_length = 100)
	precio_iva = models.FloatField(null=True, blank=True)
	descripcion = RichTextField("Descripción del producto", null=True, blank=True)
	# TODO: change for foreign
	marca = models.CharField(max_length = 100)
	presentacion = models.CharField(max_length = 10)
	codigo_barras = models.CharField(max_length = 20)
	especificaciones = RichTextField("Especificaciones", null=True, blank=True)
	empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, null=True)
	fecha_creacion = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	activo = models.BooleanField("¿Activo?", default=True)

	def __str__(self):
		return self.nombre

class ProductoFoto(models.Model):
	descripcion = models.CharField(max_length = 20)
	foto = ProcessedImageField(upload_to='producto/%s',
		processors=[ResizeToFill(400, 400)],
		format='WEBP',
		options={'quality': 80},
		null = False,
		blank = False)
	principal = models.BooleanField("¿Es la principal?", default=True)
	producto = models.ForeignKey(Producto, on_delete=models.CASCADE, null=True, related_name='fotos')

	def __str__(self):
		return str(self.descripcion)

class ProductoVideo(models.Model):
	url = models.URLField(null=True, blank=True, max_length = 150)
	producto = models.ForeignKey(Producto, on_delete=models.CASCADE, null=True, related_name='videos')

	def __str__(self):
		return str(self.url)
