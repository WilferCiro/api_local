from django.urls import path

from app.views.public import *
from app.views import index
#from app.views import private

urlpatterns = [
	path('api/public/empresa', empresa.empresaView.as_view(), name='empresa'),
	path('api/public/producto', producto.productoView.as_view(), name='producto'),
	path('api/public/tipoempresa', tipoempresa.tipoempresaView.as_view(), name='tipoempresa'),
	path('api/public/banner', banner.bannerView.as_view(), name='banner'),
	path('api/public/ciudad', ciudad.ciudadView.as_view(), name='ciudad'),
	path('api/public/departamento', departamento.departamentoView.as_view(), name='departamento'),
	path('api/public/pais', pais.paisView.as_view(), name='pais'),
	path('', index.indexView.as_view(), name='index'),
	#path('api/private/', private.publicView, name='private')
]
