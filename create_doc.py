from django.conf import settings
import myapp.settings as app_settings
import datetime
from django.db.models import Q
from django.core.mail import send_mail
import json

settings.configure(INSTALLED_APPS=app_settings.INSTALLED_APPS,DATABASES=app_settings.DATABASES, STATIC_URL=app_settings.STATIC_URL)

import django
django.setup()
from app.views.utils.serviciosCRUD import allServicios

f = open("docs/source/template.rst", "r")
template = f.read()
f.close()

for key_serv in allServicios:
	newTemp = template
	newTemp = newTemp.replace("{nombre_servicio}", key_serv)

	f = open("docs/source/Documentacion/" + key_serv + ".rst", "w+")
	dataW = newTemp.split("---")[0]

	block = newTemp.split("---")[1].split("---")[0]
	if "get" in allServicios[key_serv]:
		GET = allServicios[key_serv]["get"]
		for key in GET:
			block = newTemp.split("---")[1].split("---")[0]
			body = {
				"modelo" : key,
				"tipo_servicio" : key_serv,
			}
			if "external_conditions" in GET[key] and GET[key]["external_conditions"] != False:
				body["campos"] = {}
				for cond in GET[key]["external_conditions"]:
					body["campos"][cond] = "(opcional)"
			body = json.dumps(body, indent='\t\t').replace("}", "\t}")
			dataW += block.replace("{modelo}", key).replace("{metodo}", "GET").replace("{body}", str(body))


	f.write(dataW)
	f.close()
