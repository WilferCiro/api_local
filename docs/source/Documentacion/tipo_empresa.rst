*****************************
tipo_empresa
*****************************


nombres
******************

**Método:** GET

**Body**

.. code-block:: json

	{
		"modelo": "nombres",
		"tipo_servicio": "tipo_empresa",
		"campos": {
				"pk": "(opcional)"
			}
	}
	

todo
******************

**Método:** GET

**Body**

.. code-block:: json

	{
		"modelo": "todo",
		"tipo_servicio": "tipo_empresa",
		"campos": {
				"pk": "(opcional)"
			}
	}
	
