.. api_local documentation master file, created by
   sphinx-quickstart on Mon Apr 12 19:36:29 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to api_local's documentation!
=====================================

La URL para la parte pública es: /api/public

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. toctree::
	:glob:
	:numbered:
	:caption: Api

	Documentacion/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
